extends CollisionObject2D

var _up
var _right
var _down
var _left

var grid_size = 25
var tick_rate = 0.5
var move_cooldown = 0

var direction = 0

var bodies = []

var ideal_size = 10

var body_scene
var apple_scene

# Called when the node enters the scene tree for the first time.
func _ready():
	_up = "snak_move_up"
	_down = "snak_move_down"
	_left = "snak_move_left"
	_right = "snak_move_right"
	position.x += grid_size / 2.0 + 10 * grid_size
	position.y += grid_size / 2.0 + 10 * grid_size
	body_scene = preload("res://body.tscn")
	apple_scene = preload("res://apple.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_cooldown += delta
	
	handle_input()
	
	if (move_cooldown >= tick_rate):
		move_cooldown = 0
		do_action()

func handle_input():
	var inputX = Input.get_action_strength(_right) - Input.get_action_strength(_left)
	var inputY = Input.get_action_strength(_down) - Input.get_action_strength(_up)
	
	if inputX > 0:
		direction = 0
	elif inputX < 0:
		direction = 2
	elif inputY > 0:
		direction = 1
	elif inputY < 0:
		direction = 3

func do_action():
	handle_size()
	handle_direction()
	
func handle_size():
	bodies.insert(0, body_scene.instance())
	get_parent().add_child(bodies[0])
	bodies[0].translate(Vector2(position.x, position.y))
	
	while bodies.size() > ideal_size:
		bodies[bodies.size() - 1].queue_free()
		bodies.remove(bodies.size() - 1)

func handle_direction():
	match (direction):
		0:
			position.x += grid_size
		1:
			position.y += grid_size
		2:
			position.x -= grid_size
		3:
			position.y -= grid_size
	
func _on_head_area_entered(area):
	if "body" in area.name:
		ideal_size = 0
	if "apple" in area.name:
		ideal_size += 1
		area.set_random_position()
