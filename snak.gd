extends Node2D

var head_scene
var apple_scene

func _ready():
	head_scene = preload("res://head.tscn")
	add_child(head_scene.instance())
	apple_scene = preload("res://apple.tscn")
	set_apple()

func set_apple():
	var apple = apple_scene.instance()
	add_child(apple)
	apple.set_random_position()

func reset():
	pass
