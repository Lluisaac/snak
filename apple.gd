extends Area2D

var rng

func _ready():
	rng = RandomNumberGenerator.new()
	rng.randomize()

func set_random_position():
	var pos_x = rng.randi_range(0, 24)
	var pos_y = rng.randi_range(0, 15)
	
	position.x = 12.5 + 25 * pos_x
	position.y = 12.5 + 25 * pos_y
	
	print_debug(pos_x)
	print_debug(pos_y)
